<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>DataObjectDelegate</name>
    <message>
        <location filename="../dataObjectDelegate.cpp" line="+151"/>
        <location line="+35"/>
        <source>Real part</source>
        <translation>Realteil</translation>
    </message>
    <message>
        <location line="-27"/>
        <location line="+35"/>
        <source>Imaginary part</source>
        <translation>Imaginärteil</translation>
    </message>
</context>
<context>
    <name>DataObjectTable</name>
    <message>
        <location filename="../dataObjectTable.cpp" line="+429"/>
        <source>number of decimals</source>
        <translation>Anzahl Dezimalstellen</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>set number of decimals</source>
        <translation>Anzahl Dezimalstellen einstellen</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../dataobjecttablefactory.cpp" line="+75"/>
        <source>itom widget to interprete a dataObject as a table.</source>
        <translation>itom-Widget um ein Datenobjekt als Tabelle zu interpretieren.</translation>
    </message>
</context>
</TS>
