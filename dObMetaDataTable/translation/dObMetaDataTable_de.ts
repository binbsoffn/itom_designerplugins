<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>QObject</name>
    <message>
        <location filename="../dObMetaDataTablefactory.cpp" line="+75"/>
        <source>itom widget to interprete a dataObject as a table.</source>
        <translation>itom-Widget um ein Datenobjekt als Tabelle zu interpretieren.</translation>
    </message>
</context>
<context>
    <name>dObMetaDataTable</name>
    <message>
        <location filename="../dObMetaDataTable.cpp" line="+118"/>
        <location line="+73"/>
        <location line="+43"/>
        <source>no preview available</source>
        <translation>Keine Vorschau verfügbar</translation>
    </message>
    <message>
        <location line="-115"/>
        <location line="+120"/>
        <source>header</source>
        <translation>Metadaten</translation>
    </message>
    <message>
        <location line="-119"/>
        <source>tag Space</source>
        <translation>Tags</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>protocol</source>
        <translation>Protokoll</translation>
    </message>
    <message>
        <location line="+106"/>
        <source>preview</source>
        <translation>Vorschau</translation>
    </message>
    <message>
        <location line="+133"/>
        <source>Tag Space</source>
        <translation>Tags</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Protocol</source>
        <translation>Protokoll</translation>
    </message>
</context>
</TS>
