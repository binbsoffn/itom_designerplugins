======================================
**dObMetaTable** - designer Widget
======================================

=============== ========================================================================================================
**Summary**:    :plotsummary:`dObMetaTable`
**Type**:       :plottype:`dObMetaTable`
**Input**:       :plotinputtype:`dObMetaTable`
**Formats**:       :plotdataformats:`dObMetaTable`
**Features**:       :plotfeatures:`dObMetaTable`
**License**:    :plotlicense:`dObMetaTable`
**Platforms**:  Windows, Linux
**Author**:     :plotauthor:`dObMetaTable`
=============== ========================================================================================================

